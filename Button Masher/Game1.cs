﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using System;

namespace Button_Masher
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        SpriteFont main;
        SoundEffect clickSound, gameOverSound;
        Song gameMusic;
        Texture2D buttonTex;
        MouseState mousePrev;
        Rectangle buttonBbox;
        Vector2 screenCentre;
        int screenWidth, 
            screenHeight;

        int score = 0;
        int gameOverAlarm = 0;
        int gameOverAlarmLimit = 240;
        float time = 0f;
        float timeLimit = 5f;

        string startStr = "Click the button to start!";
        string playStr = "MASH THE BUTTON NOW!";
        string gameoverStr = "Game Over!";
        string restartStr = "Click the button to restart!";

        string promptStr;

        bool started = false;

        int buttonYPos;
        int buttonYMove = 5;
        Color buttonColour = Color.White;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;

            screenWidth = graphics.GraphicsDevice.Viewport.Width;
            screenHeight = graphics.GraphicsDevice.Viewport.Height;

            screenCentre = new Vector2(screenWidth / 2, screenHeight / 2);

            IsMouseVisible = true;

            time = timeLimit;
            promptStr = startStr;

            buttonYPos = (int)screenCentre.Y;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            main = Content.Load<SpriteFont>("fonts/mainSpriteFont");

            buttonTex = Content.Load<Texture2D>("graphics/button");
            buttonBbox = new Rectangle(screenWidth / 2 - buttonTex.Width / 2, screenHeight / 2 - buttonTex.Height / 2, buttonTex.Width, buttonTex.Height);

            clickSound = Content.Load<SoundEffect>("audio/buttonclick");
            gameOverSound = Content.Load<SoundEffect>("audio/gameover");

            gameMusic = Content.Load<Song>("audio/music");

            MediaPlayer.Play(gameMusic);
            MediaPlayer.IsRepeating = true;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            MouseState mouse = Mouse.GetState();

            // If mouse is clicked on the button
            if (mouse.LeftButton == ButtonState.Pressed && mousePrev.LeftButton == ButtonState.Released && buttonBbox.Contains(mouse.X, mouse.Y))
            {
                clickSound.Play();
                buttonYPos += buttonYMove;
            }
            if (mousePrev.LeftButton == ButtonState.Pressed && mouse.LeftButton == ButtonState.Released && buttonBbox.Contains(mouse.X, mouse.Y))
            {
                clickSound.Play();
                buttonYPos -= buttonYMove;

                if (time > 0)
                {
                    if (started)
                        ++score;
                    else
                    {
                        started = true;
                        promptStr = playStr;
                    }
                }
                else if (gameOverAlarm == 0)
                {
                    score = 0;
                    time = timeLimit;
                    started = true;
                    promptStr = playStr;

                }

               
            }

            // Update time
            if (started && time > 0)
            {
                time -= (float)gameTime.ElapsedGameTime.TotalSeconds;

                if (time < 0)
                {
                    time = 0;

                    started = false;
                    gameOverSound.Play();

                    promptStr = gameoverStr;
                    gameOverAlarm = gameOverAlarmLimit;

                    buttonColour = Color.Red;
                }
            }

            if (gameOverAlarm > 0 && --gameOverAlarm == 0)
            {
                promptStr = restartStr;
                buttonColour = Color.White;
            }

            mousePrev = mouse;

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Lerp(Color.DarkGray, Color.Black, 0.75f));

            // TODO: Add your drawing code here
            spriteBatch.Begin();

            // Get time string
            string timeStr = Math.Ceiling(time).ToString();

            // Get string origins
            Vector2 titleOrigin = main.MeasureString("Button Masher") / 2;
            Vector2 authorOrigin = main.MeasureString("By Jack Rennie");
            Vector2 promptOrigin = main.MeasureString(promptStr) / 2;
            Vector2 timeOrigin = main.MeasureString(timeStr) / 2;

            // Draw title
            spriteBatch.DrawString(main, "Button Masher", new Vector2(screenCentre.X, 10), Color.White, 0.0f, new Vector2(titleOrigin.X, 0), 1, SpriteEffects.None, 0);
            // Draw author name
            spriteBatch.DrawString(main, "By Jack Rennie", new Vector2(screenWidth - 10, screenHeight - 10), Color.White, 0.0f, authorOrigin, 0.65f, SpriteEffects.None, 0);
            // Draw prompt
            spriteBatch.DrawString(main, promptStr, new Vector2(screenCentre.X, screenCentre.Y + buttonTex.Height / 2 + 20), Color.White, 0.0f, new Vector2(promptOrigin.X, 0), 1, SpriteEffects.None, 0);
            // Draw score
            spriteBatch.DrawString(main, "Score: " + score, new Vector2(10, 10), Color.White, 0.0f, Vector2.Zero, 1, SpriteEffects.None, 0);
            // Draw time
            spriteBatch.DrawString(main, timeStr, new Vector2(screenCentre.X, screenCentre.Y - buttonTex.Height / 2 - 20), Color.White, 0.0f, new Vector2(timeOrigin.X, timeOrigin.Y * 2), 1, SpriteEffects.None, 0);

            // Draw button
            spriteBatch.Draw(buttonTex, new Rectangle((int)screenCentre.X, buttonYPos, buttonTex.Width, buttonTex.Height), null, buttonColour, 0.0f, new Vector2(buttonTex.Width / 2, buttonTex.Height / 2), SpriteEffects.None, 0);
                
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
